//
//  ViewController.swift
//  SwiftyCompanion
//
//  Created by Patricio GUZMAN on 10/16/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit
import ChameleonFramework
import RxSwift



class ViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var connect: UIButton!
    @IBOutlet weak var search: UIButton!
    @IBOutlet weak var backgroundImage: UIImageView!
    var networkCotroller: NetworkController = NetworkController()
    var homeViewModel: HomeViewModel = HomeViewModel()
    var userIsLogIn: Bool = false {
        didSet {
            self.setUI()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewdidload")
        self.setButtonsUI()
        self.setUI()
    }
    
    
    @IBAction func searchUser(_ sender: Any) {
        let login = searchBar.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if login != nil, login != "" {
            networkCotroller.getUserId(login: login!) { id in
                self.networkCotroller.getUser(id: id as! Int) { json in
                    print(json)
//                    performSegue(withIdentifier: "", sender: json)                  // HERE
                }
            }
        }

    }
    
    @IBAction func connectUser(_ sender: Any) {
        let webV:UIWebView = UIWebView(frame: CGRect(x: 10, y: 10, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height - 20))
        webV.loadRequest(NSURLRequest(url: NSURL(string: URLS.authorize + "?client_id=" + Constants.CLIENT_ID + "&redirect_uri=" + google! + "&response_type=code&scope=public")! as URL) as URLRequest)
        webV.delegate = self as UIWebViewDelegate;
        self.view.addSubview(webV)
        //        networkCotroller.loginUser()
    }
    
    func setUI() {
        if self.userIsLogIn {
            searchBar.isHidden = false
            search.isHidden = false
            connect.isHidden = true
        }
        else {
            searchBar.isHidden = true
            search.isHidden = true
            connect.isHidden = false
        }
    }
    
    func setButtonsUI() {
        search.setTitle("Search", for: .normal)
        search.layer.cornerRadius = 4
        search.layer.backgroundColor = UIColor.flatRed.cgColor
        search.layer.borderColor = UIColor.flatRedDark.cgColor
        search.layer.borderWidth = 2
        search.setTitleColor(UIColor.flatGreenDark, for: .normal)
        connect.setTitle("Connect", for: .normal)
        connect.layer.cornerRadius = 4
        connect.layer.backgroundColor = UIColor.flatRed.cgColor
        connect.layer.borderColor = UIColor.flatRedDark.cgColor
        connect.layer.borderWidth = 2
        connect.setTitleColor(UIColor.flatGreenDark, for: .normal)
    }
    
    @IBAction func deleteUserdefaults(_ sender: Any) {
        print("token and code were deleted ")
        UserDefaults.standard.removeObject(forKey: "token")
        UserDefaults.standard.removeObject(forKey: "code")
        connect.isHidden = false
    }
    
}

extension ViewController: UIWebViewDelegate {
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let host = request.url?.host {
            if host == "www.google.fr" {
                if let code = request.url!.absoluteString.components(separatedBy: "&")[0].components(separatedBy: "=")[1] as? String {
                    networkCotroller.code = code
                    networkCotroller.getAccessToken(code: code) { _ in
                        self.userIsLogIn = true
                        webView.removeFromSuperview()
                    }
                }
                return false
            }
        }
        return true
    }
    
}

