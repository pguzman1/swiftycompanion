//
//  NetworkController.swift
//  SwiftyCompanion
//
//  Created by Patricio GUZMAN on 10/16/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct URLS {
    static let base = "https://api.intra.42.fr"
    static let apiv2 = URLS.base + "/v2"
    static let getId = URLS.base + "/users?"
    static let authorize = URLS.base + "/oauth/authorize"
    static let token = URLS.base + "/oauth/token"
}

let redirect_uri = "swt://log".addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
let google = "https://www.google.fr".addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)

class NetworkController {
    let url42 = "https://api.intra.42.fr/v2/users"
    var code: String = ""
    var token: String = ""
    
    func getUserId(login: String, callback:@escaping ((Any) -> Void)) {
        print("getUserId")
        var parameters: [String: Any] = [:]
        parameters["access_token"] = token
        parameters["filter[login]"] = login
        Alamofire.request(URLS.apiv2 + "/users", parameters: parameters).responseJSON(completionHandler: { response in
            let json = JSON(response.result.value!)
            let id = json[0]["id"]
            callback(id.intValue)
        })
    }
    
    func getUser(id: Int, callback:@escaping ((Any) -> Void)) {
        print("getUser")
        var parameters: [String: Any] = [:]
        parameters["access_token"] = token
        Alamofire.request(URLS.apiv2 + "/users/\(id)", parameters: parameters).responseJSON(completionHandler: { response in
            let json = JSON(response.result.value!)
            callback(json)
        })
    }
    
    func loginUser() {
        print("loginUser")
        UIApplication.shared.open(NSURL(string: URLS.authorize + "?client_id=" + Constants.CLIENT_ID + "&redirect_uri=" + redirect_uri! + "&response_type=code&scope=public")! as URL, options: [:], completionHandler: nil)
    }
    
    func getAccessToken(code: String, callback:@escaping ((String) -> Void)) {
        print("getAccessToken")
        var parameters: [String: Any] = [:]
        parameters["grant_type"] = "authorization_code"
        parameters["client_id"] = Constants.CLIENT_ID
        parameters["client_secret"] = Constants.CLIENT_SECRET
        parameters["code"] = code
        parameters["redirect_uri"] = google
        let request = NSURLRequest(url: URL(string: URLS.token)!)
        Alamofire.request(request.url!, method: .post, parameters: parameters, headers: request.allHTTPHeaderFields).responseJSON(completionHandler: { response in
            if let json = response.result.value as? [String: Any] {
                print(json)
                if let token = json["access_token"] {
                    print("token was saved")
                    self.token = token as! String
                    UserDefaults.standard.set(token, forKey: "token")
                    callback(token as! String)
                }
            }
        })
    }
}
